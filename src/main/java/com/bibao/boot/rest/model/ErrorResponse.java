package com.bibao.boot.rest.model;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {
	private String errorMessage;
	private List<String> errorDetails;
	
	public ErrorResponse() {
		errorDetails = new ArrayList<>();
	}
	public ErrorResponse(String errorMessage, List<String> errorDetails) {
		this.errorMessage = errorMessage;
		this.errorDetails = errorDetails;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public List<String> getErrorDetails() {
		return errorDetails;
	}
	public void setErrorDetails(List<String> errorDetails) {
		this.errorDetails = errorDetails;
	}
}
