package com.bibao.boot.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.boot.dao.PersonDao;
import com.bibao.boot.model.Person;

@RestController
@RequestMapping("/person")
public class PersonRestService {
	@Autowired
	private PersonDao personDao;
	
	@GetMapping("/{id}")
	public Person findPersonById(@PathVariable("id") int id) {
		return personDao.findById(id);
	}
	
	@GetMapping
	public List<Person> findAllPerson() {
		return personDao.findAll();
	}
	
	@PostMapping(consumes="application/json", produces="application/json")
	public void savePerson(@RequestBody Person person) {
		personDao.save(person);
	}
	
	@PutMapping(consumes="application/json", produces="application/json")
	public void updatePerson(@RequestBody Person person) {
		personDao.update(person);
	}
	
	@DeleteMapping("/{id}")
	public void deletePersonById(@PathVariable("id") int id) {
		personDao.deleteById(id);
	}
}
