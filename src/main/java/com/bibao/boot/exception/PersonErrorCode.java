package com.bibao.boot.exception;

public enum PersonErrorCode {
	NOT_FOUND("Person with id %2d not found"),
	ALREADY_EXISTING("Person with id %2d already exists"),
	NON_EXISTING("Person with id %2d does not exist");
	
	private String message;
	
	private PersonErrorCode(String message) {
		this.message = message;
	}
	
	public String getMessage(int id) {
		return String.format(message, id);
	}
	
	public String getMessage() {
		return message;
	}
}
