package com.bibao.boot.exception;

public class PersonException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PersonException() {
		super();
	}
	
	public PersonException(PersonErrorCode code) {
		super(code.getMessage());
	}
	
	public PersonException(PersonErrorCode code, int id) {
		super(code.getMessage(id));
	}
}
