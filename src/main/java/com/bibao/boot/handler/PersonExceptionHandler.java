package com.bibao.boot.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.bibao.boot.rest.model.ErrorResponse;

@ControllerAdvice
public class PersonExceptionHandler {
	@ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception exception, WebRequest request) {
        List<String> exceptionDetails = new ArrayList<>();
        exceptionDetails.add(exception.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Server Error", exceptionDetails);
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
