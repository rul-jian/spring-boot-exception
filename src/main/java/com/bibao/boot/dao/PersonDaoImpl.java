package com.bibao.boot.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import com.bibao.boot.exception.PersonErrorCode;
import com.bibao.boot.exception.PersonException;
import com.bibao.boot.model.Person;

@Repository
public class PersonDaoImpl implements PersonDao {
	private static Map<Integer, Person> personMap;
	
	@PostConstruct
	private void init() {
		personMap = new HashMap<>();
		String[] firstNames = {"Alice", "Bob", "Steven"};
		String[] lastNames = {"Liu", "Zhu", "Zhang"};
		for (int i=0; i<3; i++) {
			Person person = new Person();
			person.setId(i+1);
			person.setFirstName(firstNames[i]);
			person.setLastName(lastNames[i]);
			personMap.put(person.getId(), person);
		}
	}
	
	@Override
	public Person findById(int id) {
		Person person = personMap.get(id);
		if (person==null) throw new PersonException(PersonErrorCode.NOT_FOUND, id);
		return person;
	}

	@Override
	public List<Person> findAll() {
		return personMap.values().stream().map(p -> p).collect(Collectors.toList());
	}

	@Override
	public void save(Person person) {
		if (personMap.containsKey(person.getId())) {
			throw new PersonException(PersonErrorCode.ALREADY_EXISTING, person.getId());
		}
		person.setId(getMaxId() + 1);
		personMap.put(person.getId(), person);
	}

	@Override
	public void update(Person person) {
		if (personMap.containsKey(person.getId())) {
			personMap.put(person.getId(), person);
		} else {
			throw new PersonException(PersonErrorCode.NON_EXISTING, person.getId());
		}
	}

	@Override
	public void deleteById(int id) {
		if (personMap.containsKey(id)) {
			personMap.remove(id);
		} else {
			throw new PersonException(PersonErrorCode.NON_EXISTING, id);
		}

	}

	private int getMaxId() {
		return personMap.keySet().stream().mapToInt(v -> v).max().orElse(1);
	}
}
