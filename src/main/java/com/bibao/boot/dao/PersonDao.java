package com.bibao.boot.dao;

import java.util.List;

import com.bibao.boot.model.Person;

public interface PersonDao {
	public Person findById(int id);
	public List<Person> findAll();
	public void save(Person person);
	public void update(Person person);
	public void deleteById(int id);
}
